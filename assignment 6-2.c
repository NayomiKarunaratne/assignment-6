#include<stdio.h>
int fibonacciSeq(int num);

int main()
{
    int terms;

    printf("Enter terms: ");
    scanf("%d", &terms);

    for(int n = 0; n <= terms; n++)
    {
        printf("%d ", fibonacciSeq(n));
    }

    return 0;
}

int fibonacciSeq(int num)
{

    if(num == 0 || num == 1)
    {
        return num;
    }

    else
    {
        return fibonacciSeq(num-1) + fibonacciSeq(num-2);
    }

}
